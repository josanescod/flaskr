from flask import (
    Flask, Blueprint, flash, g, redirect, render_template, request, url_for
)
from werkzeug.exceptions import abort
from werkzeug.utils  import secure_filename
import os

from flaskr.auth import login_required
from flaskr.db import get_db

bp = Blueprint('blog', __name__)


@bp.route('/')
def index():
    db = get_db()
    posts = db.execute(
        'SELECT p.id, title, body, created, author_id, username, image'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' ORDER BY created DESC'
    ).fetchall()
    return render_template('blog/index.html', posts=posts)


# muestra info de administracion
@bp.route('/admin')
@login_required
def admin():
    if g.user['admin'] != 1:
        return redirect(url_for('blog.index'))
    else:
        db = get_db()
        usuarios = db.execute(
            'SELECT username,id'
            ' FROM user ').fetchall()
        numposts = db.execute(
            'SELECT COUNT (title)'
            'FROM post').fetchone()
        numusers = db.execute(
            'SELECT COUNT (username)'
            'FROM user').fetchone()

        return render_template('admin/admin.html', usuarios=usuarios, numposts=numposts,
                               numusers=numusers)


@bp.route('/create', methods=('GET', 'POST'))
@login_required
def create():
    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        image = request.files['image']
        print (image.filename)
        error = None

        if not title:
            error = 'Escribe un titulo.'

        if error is not None:
            flash(error)
        else:
            
            print ('hacemos insert')
            db = get_db()
            db.execute(
                'INSERT INTO post (title, body, image, author_id)'
                'VALUES (?, ?, ?, ?)',
                (title, body, image.filename, g.user['id'])
            )
            db.commit()
            # guardando fichero jpg del post
            try:
                APP_ROOT = os.path.dirname(os.path.abspath(__file__))
                target = os.path.join(APP_ROOT, 'static/images')
                filename = image.filename
                print (target)
                print (image.filename)
                destination = "/".join([target,filename])
                image.save(destination)
               
            except:
                print('no hay imagen')
            return redirect(url_for('blog.index'))
        

    return render_template('blog/create.html')


def get_post(id, check_author=True):
    post = get_db().execute(
        'SELECT p.id, title, body, created, author_id, username, image'
        ' FROM post p JOIN user u ON p.author_id = u.id'
        ' WHERE p.id = ?',
        (id,)
    ).fetchone()

    if check_author and post['author_id'] != g.user['id'] and g.user['admin'] == 1:
        return post

    if post is None:
        abort(404, "Post id {0} no existe.".format(id))

    if check_author and post['author_id'] != g.user['id']:
        abort(403)

    return post


@bp.route('/<int:id>/update', methods=('GET', 'POST'))
@login_required
def update(id):
    post = get_post(id)

    if request.method == 'POST':
        title = request.form['title']
        body = request.form['body']
        image = request.files['image'] #importante me daba error porque es request.files !!
        error = None

        if not title:
            error = 'Escribe un titulo.'

        if error is not None:
            flash(error)
        else:
            db = get_db()
            # primero buscar imagen anterior
            nombreAnterior = db.execute(
                'SELECT image FROM post WHERE id= ?',[id]).fetchall()
            

            # eliminar fichero jpg anterior 
            try:
                APP_ROOT = os.path.dirname(os.path.abspath(__file__))
                target = os.path.join(APP_ROOT, 'static/images')
                nombreAnterior = nombreAnterior[0][0]
                print ('eliminando: '+ nombreAnterior)
                os.remove(os.path.join(
                    target, nombreAnterior))
               
            except:
                print('no hay imagen')

            # actualizamos la nueva imagen en la bdd
            db.execute(
                'UPDATE post SET title = ?, body = ?, image = ? '
                ' WHERE id = ?',
                (title, body,image.filename, id)
            )
            db.commit()
            # guardando nuevo fichero jpg del post
            try:
                APP_ROOT = os.path.dirname(os.path.abspath(__file__))
                target = os.path.join(APP_ROOT, 'static/images')
                filename = image.filename
                print (target)
                print (image.filename)
                destination = "/".join([target,filename])
                image.save(destination)
               
            except:
                print('no hay imagen')
            return redirect(url_for('blog.index'))

    return render_template('blog/update.html', post=post)


@bp.route('/<int:id>/delete', methods=('POST',))
@login_required
def delete(id):
    get_post(id)
    db = get_db()
    db.execute('DELETE FROM post WHERE id = ?', (id,))
    db.commit()
    return redirect(url_for('blog.index'))


@bp.route('/<int:id>/deleteuser', methods=('POST',))
@login_required
def deleteuser(id):
    print('borrando usuario con id: '+str(id))
    db = get_db()
    db.execute('DELETE FROM user WHERE id = ?', (id,))
    db.execute('DELETE FROM post WHERE author_id = ?', (id,))
    db.commit()

    return redirect(url_for('blog.admin'))

#mostrar post ampliado
@bp.route('/<int:id>/zoom')
def zoom(id):
    post = get_post(id,False)
    return render_template('blog/zoom.html', post=post)
