# esto es un blueprint o plano la aplicacion tendra 2 uno de autorizacion de usuario y otro para posts del blog

import functools

from flask import (
    Blueprint, flash, g, redirect, render_template, request, session, url_for
)
from werkzeug.security import check_password_hash, generate_password_hash

from flaskr.db import get_db

bp = Blueprint('auth', __name__, url_prefix='/auth')

# login requerido, se usa un decorador para comprobar si el suuario estár cargado
def login_required(view):
    @functools.wraps(view)
    def wrapped_view(**kwargs):
        if g.user is None:
            return redirect(url_for('auth.login'))

        return view(**kwargs)

    return wrapped_view

# antes de que se ejecute cualquier funcion comprueba si hay una sesion de usuario
@bp.before_app_request
def load_logged_in_user():
    user_id = session.get('user_id')

    if user_id is None:
        g.user = None
    else:
        g.user = get_db().execute(
        'SELECT * FROM user WHERE id = ?', (user_id,)
        ).fetchone()


# vista auth/register    
@bp.route('/register', methods=('GET', 'POST'))
def register():
    db = get_db()
    
    numadmins = db.execute (
            'SELECT SUM (admin)'
            'FROM user'
            
            ).fetchone()
    
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        try:
            admin = request.form['admin']
            int(admin)
            
        except:
            print ('si no selecciona opcion no tiene permiso')
            admin=0
            
        
            
        
        
        
        print (admin)
        db = get_db()
        error = None

        if not username:
            error = 'Es necesario introducir un nombre.'
        elif not password:
            error = 'Es necesario introducir un password.'
        
        elif db.execute(
            'SELECT id FROM user WHERE username = ?', (username,)
        ).fetchone() is not None:
            error = 'El usuario {} ya existe.'.format(username)

        if error is None:
            db.execute(
                'INSERT INTO user (username, password,admin) VALUES (?, ?, ?)',
                (username, generate_password_hash(password),admin)
             )
            db.commit()
            return redirect(url_for('auth.login'))

        flash(error)

    return render_template('auth/register.html',numadmins=numadmins)

# vista auth/login
@bp.route('/login', methods=('GET', 'POST'))
def login():
    if request.method == 'POST':
        username = request.form['username']
        password = request.form['password']
        db = get_db()
        error = None
        user = db.execute(
        'SELECT * FROM user WHERE username = ?', (username,)
         ).fetchone()

        if user is None:
           error = 'Nombre de usuario incorrecto.'
        elif not check_password_hash(user['password'], password):
           error = 'Contraseña incorrecta.'

        if error is None:
           session.clear()
           session['user_id'] = user['id']
           return redirect(url_for('index'))

        flash(error)

    return render_template('auth/login.html')


# logout , elimina una sesion de usuario
@bp.route('/logout')
def logout():
    session.clear()
    return redirect(url_for('index'))


