import os

from flask import (Flask, render_template, g, redirect, url_for)
from flaskr.auth import login_required
from flaskr.db import get_db


def create_app(test_config=None):

    # crea y configura la aplicacion
    app = Flask(__name__, instance_relative_config=True)
    app.config.from_mapping(
        SECRET_KEY='test',
        DATABASE=os.path.join(app.instance_path, 'flaskr.sqlite'),
        UPLOAD_FOLDER = 'static/images/',
    )
    # configuracion directorio guardar ficheros
    app.config['UPLOAD_FOLDER'] = 'static/images/'
    if test_config is None:
        # carga la instancia config, si existe, cuando no se hace testing
        app.config.from_pyfile('config.py', silent=True)
    else:
        # carga test config si es pasado
        app.config.from_mapping(test_config)

    # asegurarse que la carpeta de instancia existe
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    # un simple hello world
    @app.route('/hello')
    def hello():
        db = get_db()
        image = db.execute(
            'SELECT image'
            ' FROM post ').fetchall()
        print (image[0])
        return 'Hello, World! :D'

    from flaskr import db
    db.init_app(app)
    from flaskr import auth
    app.register_blueprint(auth.bp)
    from . import blog
    app.register_blueprint(blog.bp)
    app.add_url_rule('/', endpoint='index')

    return app
